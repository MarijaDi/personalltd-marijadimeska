@extends('layouts.app')

@section('content')
<div class="container mt-5 text-center">
    @if(count($calls) == 0)
    <p class="call_error">There are no calls recorded yet</p>
    @else

    @include('parts.messages')
    <table class="table table-hover mb-5 table-calls">
        <thead>
            <tr class="table-success">
                <th scope="col">#</th>
                <th scope="col">User name</th>
                <th scope="col">Client name</th>
                <th scope="col">Client type</th>
                <th scope="col">Date of call</th>
                <th scope="col">Duration</th>
                <th scope="col">Type of call</th>
                <th scope="col">External call score</th>
            </tr>
        </thead>
        <tbody>
            @foreach($calls as $key => $call)
            <tr onclick="window.location='{{route("read_call", ["id" => encrypt($call->id)])}}';" class="table_row"  data-toggle="tooltip" data-placement="top" title="See call">
                <th scope="row">{{$calls->firstItem() + $key}}</th>
                <td>{{$call->user}}</td>
                <td>{{$call->client}}</td>
                <td>{{$call->client_type}}</td>
                <td>{{date('F jS, Y; H:i:s', strtotime($call->date))}}</td>
                <td>{{$call->duration}}</td>
                <td>{{$call->type_of_call}}</td>
                <td>{{$call->external_call_score}}</td>
            </tr>

            @endforeach
        </tbody>
    </table>
    @if($calls->lastPage()>1)

    <nav aria-label="...">
        <ul class="pagination">
            <li {{$calls->currentPage() == 1 ? 'class=disabled' : ''}}><a href="{{$calls->previousPageUrl()}}"
                    aria-label="Previous" data-toggle="tooltip" data-placement="top" title="Previous"><span
                        aria-hidden="true">&laquo;</span></a></li>


            @if($calls->currentPage() > 1)
            <li {{$calls->currentPage() == 1 ? 'class=active' : ''}}><a href="{{route('landing')}}"> 1 <span
                        class="sr-only">(current)</span></a></li>
            @endif
            @if($calls->currentPage() > 2)
            <li {{$calls->currentPage() == 2 ? 'class=active' : ''}}><a href="{{route('landing')}}?page=2"> 2</a></li>
            @endif
            @if($calls->currentPage() > 3)
            <li><a>...</a></li>
            @endif
            @for($calls->currentPage() + 10 > $calls->lastPage() ? $i =
            $calls->lastPage() - 11 : $i = $calls->currentPage(); $calls->currentPage() + 10 > $calls->lastPage() ?
            $i
            <= $calls->lastPage() : $i
                <=$calls->currentPage() + 10; $i++)

                    @if($i > 0)
                    <li {{$calls->currentPage() == $i ? 'class=active' : ''}}><a
                            href="{{route('landing')}}?page={{$i}}">{{$i}}</a></li>
                    @endif
                    @endfor

                    @if($calls->lastPage()-11 > $calls->currentPage())
                    <li><a>...</a></li>
                    @endif
                    @if($calls->lastPage()-10 > $calls->currentPage())

                    <li {{$calls->currentPage() == $i ? 'class=active' : ''}}><a
                            href="{{route('landing')}}?page={{$calls->lastPage()}}">{{$calls->lastPage()}}</a>
                    </li>
                    @endif
                    <li {{$calls->currentPage() == $calls->lastPage() ? 'class=disabled' : ''}}><a
                            href="{{$calls->nextPageUrl()}}" aria-label="Next" data-toggle="tooltip"
                            data-placement="top" title="Next"><span aria-hidden="true">&raquo;</span></a>
                    </li>
        </ul>
    </nav>
    @endif
    @endif
</div>
@endsection