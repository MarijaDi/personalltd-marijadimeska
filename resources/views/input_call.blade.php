@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if(\Request::is('call/update'))
            <a href="{{route('read_call', ['id' => encrypt($call->id)])}}"><button type="button" class="btn btn-default btn-sm button_go_back" aria-label="Left Align"  data-toggle="tooltip" data-placement="top" title="Go back">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            </button>
            </a>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Call {{\Request::is('call/update') ? "no.$call->id" : ''}} details

                </div>
                <div class="panel-body">
                    <form method="POST"
                        action="{{\Request::is('call/update') ? route('edit_call', ['id' => encrypt($call->id)]) : route('save_call') }}">
                        @csrf
                        <div class="form-group {{ $errors->has('user') ? ' has-error' : '' }}">
                            <label for="user" class="call_label">User</label>
                            <select name="user" id="user"
                                class="call_select {{ $errors->has('user') ? ' border_red' : '' }}">
                                <option value="">User</option>
                                @foreach($users as $user)
                                <option value="{{$user}}" @if(\Request::is('call/update'))
                                    {{$call->user == $user ? 'selected' : ''}} @endif
                                    {{old('user') == $user ? "selected" : ''}}>{{$user}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('user'))
                            <span class="help-block">
                                <strong>{{ $errors->first('user') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('client') ? ' has-error' : '' }}">
                            <label for="client" class="call_label">Client</label>
                            <select name="client" id="client"
                                class="call_select {{ $errors->has('client') ? ' border_red' : '' }}">
                                <option value="">Client</option>
                                @foreach($clients as $client)
                                <option value="{{$client}}" @if(\Request::is('call/update'))
                                    {{$call->client == $client ? 'selected' : ''}} @endif
                                    {{old('client') == $client ? "selected" : ''}}>{{$client}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('client'))
                            <span class="help-block">
                                <strong>{{ $errors->first('client') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('client_type') ? ' has-error' : '' }}">

                            <label for="client_type" class="call_label">Client type</label>
                            <input type="text" class="call_input {{ $errors->has('client_type') ? ' border_red' : '' }}"
                                name='client_type' id="client_type"
                                value="{{\Request::is('call/update') &&  !old('client_type') ? $call->client_type : old("client_type")}}">

                            @if ($errors->has('client_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('client_type') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="date" class="call_label">Date</label>
                            <input type="datetime-local" name="date" id="date"
                                class="call_date_input {{ $errors->has('date') ? ' border_red' : '' }}"
                                value="{{\Request::is('call/update') &&  !old('date') ? Carbon\Carbon::parse($call->date)->format('Y-m-d\TH:i:s') : old('date', date('Y-m-d\TH:i:s'))}}"
                                max="{{Carbon\Carbon::now()->format('Y-m-d\TH:i:s')}}">
                            @if ($errors->has('date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('date') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('duration') ? ' has-error' : '' }}">
                            <label for="duration" class="call_label">Duration</label>
                            <input type="number" class="call_input {{ $errors->has('duration') ? ' border_red' : '' }}"
                                name='duration' id="duration"
                                value="{{\Request::is('call/update') &&  !old('duration') ? $call->duration : old("duration")}}">


                            @if ($errors->has('duration'))
                            <span class="help-block">
                                <strong>{{ $errors->first('duration') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('type_of_call') ? ' has-error' : '' }}">
                            <label for="type_of_call" class="call_label">Type of call</label>
                            <select name="type_of_call" id="type_of_call"
                                class="call_select {{ $errors->has('type_of_call') ? ' border_red' : '' }}">
                                <option value="">Type of call</option>
                                @foreach($types_of_call as $type_of_call)
                                <option value="{{$type_of_call}}" @if(\Request::is('call/update'))
                                    {{$call->type_of_call == $type_of_call ? 'selected' : ''}} @endif
                                    {{old('type_of_call') == $type_of_call ? "selected" : ''}}>{{$type_of_call}}
                                </option>
                                @endforeach
                            </select>

                            @if ($errors->has('type_of_call'))
                            <span class="help-block">
                                <strong>{{ $errors->first('type_of_call') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('external_call_score') ? ' has-error' : '' }}">
                            <label for="external_call_score" class="call_label">External call score</label>
                            <input type="number"
                                class="call_input {{ $errors->has('external_call_score') ? ' border_red' : '' }}"
                                name='external_call_score' id="external_call_score"
                                value="{{\Request::is('call/update') &&  !old('external_call_score') ? $call->external_call_score : old("external_call_score")}}">

                            @if ($errors->has('external_call_score'))
                            <span class="help-block">
                                <strong>{{ $errors->first('external_call_score') }}</strong>
                            </span>
                            @endif
                        </div>


                        <button type="submit"
                            class="btn btn-primary btn_add_call">{{\Request::is('call/update') ? "Update call" : "Add call" }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection