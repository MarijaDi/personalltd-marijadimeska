@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="col-md-6">

        <table class="table table-hover mb-5 " id="paginateTable">
            <thead>
                <tr class="table-success">
                    <th scope="col">#</th>
                    <th scope="col">User name</th>
                </tr>
            </thead>
            <tbody>
                @foreach($allusers as $user)
                <tr class="table_row view_option"
                    onclick="window.location='{{route("show_user", ["id" => encrypt($user->id)])}}';">
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$user->user}}
                        <span class="glyphicon glyphicon-eye-open view_glyph" aria-hidden="true"></span>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection