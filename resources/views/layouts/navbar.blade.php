<nav class="navbar navbar-default navbarStyle">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 navigationMenu">
                <p class="navbarBrand" onclick="window.location='{{route("landing")}}';">Personal Ltd | Marija Dimeska
                </p>
            </div>
            <div class="col-md-9 col-sm-6 navbarButtons text-right">
            <a href="{{route('upload_csv')}}"><button type="button" class="btn btn-success btn-sm">Add a file</button></a>
                <a href="{{route('create_call')}}"><button type="button" class="btn btn-primary btn-sm">Add a
                        call</button></a>

                <a href="{{route('see_users')}}"><button type="button" class="btn btn-default btn-sm">See all users</button></a>

            </div>
        </div>
    </div>
</nav>