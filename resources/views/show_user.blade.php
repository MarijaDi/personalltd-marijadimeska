@extends('layouts.app')

@section('content')


<div class="container show-user-container">
    <div class="row read_call_row">
        <div class="col-md-10 col-md-offset-1">
        <a href="{{route('see_users')}}"><button type="button" class="btn btn-default btn-sm button_go_back" aria-label="Left Align"  data-toggle="tooltip" data-placement="top" title="Go back">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            </button>
            </a>
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-call">User details </div>
                <div class="panel-body">
                    <p class="read_call_info">Firstname: <span> {{$firstname ? $firstname : '/'}} </span></p>
                    <p class="read_call_info">Lastname: <span> {{$lastname ? $lastname : '/'}} </span></p>
                    <p class="read_call_info">Average score: <span> {{$score ? $score : '/'}} </span></p>
                    <table class="table table-hover table_five">
                        <thead>
                            <tr class="table-success">
                                <th scope="col">#</th>
                                <th scope="col">Client name</th>
                                <th scope="col">Client type</th>
                                <th scope="col">Date of call</th>
                                <th scope="col">Duration</th>
                                <th scope="col">Type of call</th>
                                <th scope="col">External call score</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($last_five_calls as $call)
                            <tr onclick="window.location='{{route("read_call", ["id" => encrypt($call->id)])}}';"
                                class="table_row" data-toggle="tooltip" data-placement="top" title="See call">
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$call->client}}</td>
                                <td>{{$call->client_type}}</td>
                                <td>{{date('F jS, Y; H:i:s', strtotime($call->date))}}</td>
                                <td>{{$call->duration}}</td>
                                <td>{{$call->type_of_call}}</td>
                                <td>{{$call->external_call_score}}</td>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <nav aria-label="..." class="text-right">
                <ul class="pagination">
                    <li {{array_key_exists($currid-1, $ids) ? '' : 'class=disabled'}}>
                        <a href='{{route("show_user", ["id" => (array_key_exists($currid-1, $ids) ? encrypt($ids[$currid-1]) : encrypt($userid))])}}'
                            aria-label="Previous call" data-toggle="tooltip" data-placement="top"
                            title="Previous call"><span aria-hidden="true">&laquo;</span></a>
                    </li>
                    <li {{array_key_exists($currid+1, $ids) ? '' : 'class=disabled'}}> <a
                            href='{{route("show_user", ["id" => (array_key_exists($currid+1, $ids) ? encrypt($ids[$currid+1]) : encrypt($userid))])}}'
                            aria-label="Next call" data-toggle="tooltip" data-placement="top" title="Next call"><span
                                aria-hidden="true">&raquo;</span></a></li>
            </nav>
        </div>
    </div>
</div>

@endsection