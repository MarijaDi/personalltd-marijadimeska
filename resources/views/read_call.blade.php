@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row read_call_row">
        <div class="col-md-8 col-md-offset-2">

            @include('parts.messages')
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-call">Call no.{{$currid+1}}
                    <a data-toggle="modal" data-target="#myModal">
                        <span class="glyphicon glyphicon-trash glyph glyph-delete" aria-hidden="true"
                            data-toggle="tooltip" data-placement="top" title="Delete"></span></a>
                    <a href="{{route('update_call', ['id' => encrypt($call->id)])}}"> <span class="glyphicon glyphicon-hand-up glyph glyph-edit" aria-hidden="true"
                            data-toggle="tooltip" data-placement="top" title="Edit"></span></a>
                </div>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal_window" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>

                                Do you want to delete this call?
                            </div>
                            <div class="modal-footer">
                                <a href="{{route('delete_call', ['id' => encrypt($call->id)])}}" class="delete_call_button"><button type="button"
                                        class="btn btn-primary">Yes</button>
                                </a> <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <p class="read_call_info">User name: <span> {{$call->user ? $call->user : '/'}} </span></p>
                    <p class="read_call_info">Client name: <span> {{$call->client ? $call->client : '/'}} </span></p>
                    <p class="read_call_info">Client type: <span> {{$call->client_type ? $call->client_type : '/'}}
                        </span></p>
                    <p class="read_call_info">Date of call:
                        <span> {{$call->date ? date('F jS, Y; H:i:s', strtotime($call->date))  : '/'}} </span>
                    </p>
                    <p class="read_call_info">Duration: <span> {{$call->duration ? $call->duration : '/'}} </span></p>
                    <p class="read_call_info">Type of call: <span> {{$call->type_of_call ? $call->type_of_call : '/'}}
                        </span></p>
                    <p class="read_call_info">External call score:
                        <span> {{$call->external_call_score ? $call->external_call_score : '/'}} </span>
                    </p>
                </div>
            </div>
            <nav aria-label="..." class="text-right">
                <ul class="pagination">
                    <li {{array_key_exists($currid-1, $ids) ? '' : 'class=disabled'}}>
                        <a href='{{route("read_call", ["id" => (array_key_exists($currid-1, $ids) ? encrypt($ids[$currid-1]) : encrypt($call->id))])}}'
                            aria-label="Previous call" data-toggle="tooltip" data-placement="top"
                            title="Previous call"><span aria-hidden="true">&laquo;</span></a>
                    </li>
                    <li {{array_key_exists($currid+1, $ids) ? '' : 'class=disabled'}}> <a
                            href='{{route("read_call", ["id" => (array_key_exists($currid+1, $ids) ? encrypt($ids[$currid+1]) : encrypt($call->id))])}}'
                            aria-label="Next call" data-toggle="tooltip" data-placement="top" title="Next call"><span
                                aria-hidden="true">&raquo;</span></a></li>
            </nav>

        </div>
    </div>
</div>

@endsection