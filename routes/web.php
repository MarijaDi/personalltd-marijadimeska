<?php

use App\Http\Controllers\CallController;
use App\Http\Controllers\CsvController;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'landing']) -> name('landing');
Route::get('/csv/upload', [CsvController::class, 'upload_csv']) -> name('upload_csv');
Route::post('csv/import', [CsvController::class, 'import_csv']) -> name('import_csv');
Route::post('/csv/process', [CsvController::class, 'process_csv']) -> name('process_csv');
Route::get('/call/create', [CallController::class, 'create_call']) -> name('create_call');
Route::post('/call/create', [CallController::class, 'save_call']) -> name('save_call');
Route::get('/call/read', [CallController::class, 'read_call']) -> name('read_call');
Route::get('/call/update', [CallController::class, 'update_call']) -> name('update_call');
Route::post('/call/edit', [CallController::class, 'edit_call']) -> name('edit_call');
Route::get('/call/delete', [CallController::class, 'delete_call']) -> name('delete_call');
Route::get('/users/list', [PageController::class, 'see_all_users']) -> name('see_users');
Route::get('/user/show', [PageController::class, 'show_user']) -> name('show_user');