<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CallUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user' => 'required',
            "client" => 'required',
            "client_type" => 'required',
            "date" => 'required|before_or_equal:now',
            "duration" => 'required|numeric', 
            "type_of_call" => 'required',
            "external_call_score" => 'required|numeric',
        ];
    }
}
