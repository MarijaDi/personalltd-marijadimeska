<?php

namespace App\Http\Controllers;

use App\Models\Call;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function landing(){

        $calls = Call::paginate(15);
        
        return view('landing', compact('calls'));
    }
   
    public function see_all_users(Request $request){

        $allusers =Call::all()->unique('user');
        
        return view('see_users', compact('allusers'));
    }

    public function show_user(Request $request){
        $user = Call::findOrFail(decrypt($request->id));

        $ids = Call::all()->unique('user')->pluck('id')->toArray();
        $currid = array_search($user->id, $ids);
        $userid = $user->id;
 
        $score =  Call::where('user', '=', $user->user)->avg('external_call_score');
        $score = round($score, 2);
        $last_five_calls = Call::where('user', '=', $user->user)->orderBy('date', 'DESC')->take(5)->get();
        $parts = explode(" ", $user->user);
        $firstname = $parts[0];
        $lastname = $parts[1];
        
        
        return view('show_user', compact('firstname', 'lastname', 'score', 'last_five_calls', 'ids', 'currid', 'userid'));
    }
}