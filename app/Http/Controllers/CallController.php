<?php

namespace App\Http\Controllers;

use App\Http\Requests\CallUploadRequest;
use App\Models\Call;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;

class CallController extends Controller
{
    public function create_call(){

        $allusers = Call::all()->pluck('user')->toArray();
        $users = array_unique($allusers);
        $allclients = Call::all()->pluck('client')->toArray();
        $clients = array_unique($allclients);
        $allcalls = Call::all()->pluck('type_of_call')->toArray();
        $types_of_call = array_unique($allcalls);
        
        return view('input_call', compact('users', 'clients', 'types_of_call'));
    }

    public function save_call(CallUploadRequest $request){

        Call::create($request->all());


        $request->session()->flash('message', 'The call was created');
        return redirect('/');
    }
    
    public function read_call(Request $request){

        $call = Call::findOrFail(decrypt($request->id));

        $ids = Call::pluck('id')->toArray();
        $currid = array_search($call->id, $ids);
        
        return view('read_call', compact('call', 'ids', 'currid'));
    }

    public function update_call(Request $request){
        $call = Call::findOrFail(decrypt($request->id));
        $allusers = Call::all()->pluck('user')->toArray();
        $users = array_unique($allusers);
        $allclients = Call::all()->pluck('client')->toArray();
        $clients = array_unique($allclients);
        $allcalls = Call::all()->pluck('type_of_call')->toArray();
        $types_of_call = array_unique($allcalls);


        return view('input_call', compact('call', 'users', 'clients', 'types_of_call'));
    }

    public function edit_call(CallUploadRequest $request){
        $input = $request->all();
        $call = Call::findOrFail(decrypt($request->id));
        $call->update($input);
        
        $request->session()->flash('message', 'The call was updated');
        return redirect("/call/read?id=$request->id");
    }

    public function delete_call(Request $request){
        $call = Call::findOrFail(decrypt($request->id));

        $call->delete();

        $nextid = Call::where('id', '>', decrypt($request->id))->orderBy('id')->first();
        $previd = Call::where('id', '<', decrypt($request->id))->orderBy('id', 'DESC')->first();
        
        $nextid ? $nextcall = $nextid : ($previd ? $nextcall = $previd : $nextcall = null);
        
        if($nextcall == null)
            return redirect('/');

        $request->session()->flash('message', 'The call was deleted');
        return redirect("/call/read?id=".encrypt($nextcall->id));
    }
}
