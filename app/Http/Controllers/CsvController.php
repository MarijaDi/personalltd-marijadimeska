<?php

namespace App\Http\Controllers;

use App\Http\Requests\CsvUploadRequest;
use App\Imports\CallImport;
use App\Models\Call;
use App\Models\CsvData;
use Illuminate\Http\Request;
use Excel;

class CsvController extends Controller
{
    public function upload_csv(){
        return view('upload_csv');
    }

    public function import_csv(CsvUploadRequest $request){
        Excel::import(new CallImport, $request->csv_file);

        $request->session()->flash('message', 'The file was added');
        return redirect('/');
    }
}